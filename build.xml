<?xml version="1.0" encoding="UTF-8"?>
<project name="${projectName}" basedir="." default="build:main">
  <!-- Properties -->
    <property name="dir.app" value="${project.basedir}/app" />
    <property name="dir.src" value="${project.basedir}/src" />
    <property name="dir.build" value="${project.basedir}/app/build" />
    <property name="dir.docs" value="${dir.build}/docs" />
    <property name="dir.docs.phpdoc" value="${dir.docs}/phpdoc" />
    <property name="dir.docs.docblox" value="${dir.docs}/docblox" />
    <property name="dir.reports" value="${dir.build}/logs" />
    <property name="dir.reports.pdepend" value="${dir.reports}/pdepend" />
    <property name="dir.reports.coverage" value="${dir.reports}/coverage" />
    <property name="php_bin_path" value="D:\php\composer\" />

  <!-- Filesets -->
    <fileset id="sourcecode" dir="${dir.src}">
        <include name="**/*.php" />
    </fileset>

  <!-- Default target -->
    <target name="build:main"
          depends="build:clean, build:prepare, build:check, build:test, build:doc"
          description="Run all test and build everything" />

 <target name="composer" description="Update composer packages with composer.phar">
  <exec logoutput="true"
        command="composer update" />
 </target>

  <!-- Doc target -->
    <target name="build:doc"
          depends="build:prepare, doc:phpdoc, doc:docblox"
          description="Generates app API documentation." />

  <!-- Check target -->
    <target name="build:check"
          depends="check:cs, check:md, check:cpd, check:depend"
          description="Analyzes app code." />

  <!-- Test target -->
    <target name="build:test"
          depends="test:unit"
          description="Executes all tests.." />

  <!-- Project build clean -->
    <target name="build:clean" description="Clean up build directories.">
        <echo msg="Cleaning build directories ..." />
        <delete dir="${dir.build}" verbose="true" />
    </target>

  <!-- Project build prepare -->
    <target name="build:prepare" description="Create build directories.">
        <echo msg="Creating build directories ..." />
        <mkdir dir="${dir.build}" />
        <mkdir dir="${dir.docs}" />
        <mkdir dir="${dir.docs.phpdoc}" />
        <mkdir dir="${dir.docs.docblox}" />
        <mkdir dir="${dir.reports}" />
        <mkdir dir="${dir.reports.coverage}" />
        <mkdir dir="${dir.reports.pdepend}" />
    </target>

  <!-- PHPDOC API documentation target -->
    <target name="doc:phpdoc" description="Generate API documentation.">
        <echo msg="Generating API documentation with PHPDoc..." />
        <phpdoc title="${phing.project.name} :: API Documentation"
                defaultpackagename="${phing.project.name}"
                destdir="${dir.docs.phpdoc}"
                output="HTML:Smarty:PHP"
                sourcecode="yes">
            <fileset refid="sourcecode" />
        </phpdoc>
    </target>

    <!-- ============================================  -->
    <!-- Target: documentacion                         -->
    <!-- ============================================  -->
    <target name="documentacion">
         <echo msg="Generating API Documentation with phpdocs ..." />
       <phpdoc2 title="Documentación del proyecto" destdir="${dir.docs.docblox}" template="responsive-twig" pharlocation="D:\php\phpdocs\phpDocumentor.phar">
       <fileset dir="src">
          <include name="**/*.php" />
       </fileset>
      </phpdoc2>
    </target>

  <!-- DocBlox API documentation target -->
    <target name="doc:docblox" description="Generate API Documentation.">
        <echo msg="Generating API Documentation with DocBlox ..." />
        <docblox title="${phing.project.name} :: API Documentation"
                 destdir="${dir.docs.docblox}"
                 quiet="false">
            <fileset refid="sourcecode" />
        </docblox>
    </target>

  <!-- Symfony2 code sniffer -->
    <target name="check:cs" description="Checks coding standard.">
        <echo msg="Checking coding standard ..." />
        <phpcodesniffer standard="Symfony2"
                        showSniffs="true"
                        showWarnings="true">
            <fileset refid="sourcecode" />
            <formatter type="checkstyle" outfile="${dir.reports}/checkstyle.xml" />
        </phpcodesniffer>
    </target>

  <!-- copy/paste detector -->
    <target name="check:cpd" description="Checks similar code blocks.">
        <echo msg="Checking similar code blocks ..." />
        <phpcpd>
            <fileset refid="sourcecode" />
            <formatter type="pmd" outfile="${dir.reports}/pmd-cpd.xml" />
        </phpcpd>
    </target>

  <!-- PHPMD Mess detector -->
    <target name="check:md" description="Generate code metrics PHPMD.">
        <echo msg="Generating code metrics ..." />
        <phpmd rulesets="codesize,unusedcode" pharlocation="D:\php\phpmd\phpmd.phar">
            <fileset refid="sourcecode" />

            <formatter type="xml" outfile="${dir.reports}/report.xml" />
        </phpmd>
        <xslt file="${dir.reports}/report.xml"
              style="${dir.reports}/report.xslt"
              tofile="${dir.reports}/report.html" />
    </target>

    <!-- ============================================  -->
    <!-- Target: QA Code revision PHPMD                -->
    <!-- ============================================  -->
   <target name="QA" description="Code revision PHPMD">
      <phpmd  rulesets="naming" pharlocation="D:\php\phpmd\phpmd.phar">
         <formatter type="html" outfile="${dir.reports}/report.xml"/>
         <fileset dir="${dir.src}">
             <include name="IncidenciaBundle**/*.php" />
         </fileset>
      </phpmd>
        <xslt file="${dir.reports}/report.xml"
              style="${dir.reports}/report.xslt"
              tofile="${dir.reports}/report.html" />
    </target>

  <target name="phpmd">
    <exec command="phpmd" passthru="true"/>
  </target>
  <target name="_phpmd">
    <mkdir dir="${phpmd.output.xml.dir}"/>
    <mkdir dir="${phpmd.output.html.dir}"/>
    <exec
      command="phpmd ${phpmd.args}"
      dir="${project.basedir}"
      passthru="true"
    />
    <xslt
      file="${phpmd.output.xml.file}"
      tofile="${phpmd.output.html.file}"
      style="${phpmd.style}"
    />
  </target>

  <!-- Code dependency -->
    <target name="check:depend" description="Checks coupling and dependency.">
        <echo msg="Checking coupling and dependency ..." />
        <phpdepend>
            <fileset refid="sourcecode" />
            <logger type="jdepend-xml" outfile="${dir.reports.pdepend}/jdepend.xml" />
            <logger type="jdepend-chart" outfile="${dir.reports.pdepend}/dependencies.svg" />
            <logger type="overview-pyramid" outfile="${dir.reports.pdepend}/overview-pyramid.svg" />
        </phpdepend>
    </target>

  <!-- Unit tests -->
    <target name="test:unit" description="Executes unit tests.1">
        <echo msg="Running unit tests ..." />
        <exec command="phpunit --log-junit ${dir.app}/phpunit_salida.xml --coverage-clover clover --coverage-html ${dir.reports.coverage}/ -c ${dir.app}"/>
    </target>

</project>
