<?php

namespace IncidenciaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use IncidenciaBundle\Entity\Tareas;
use IncidenciaBundle\Form\TareasType;
use IncidenciaBundle\Entity\Subtareas;
use IncidenciaBundle\Form\SubtareasType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $name='entradas';

        return $this->render('IncidenciaBundle:Default:index.html.twig', array('name' => $name));

    }

    public function listartareasAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tarea = $em->getRepository('IncidenciaBundle:Tareas')->getTareas();

        return $this->render('IncidenciaBundle:Default:listadotareas.html.twig', array('tareas' => $tarea));

    }

    /**
     * Muestra una tarea
     */
    public function detalletareaAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $detalletarea = $em->getRepository('IncidenciaBundle:Tareas')->find($id);

        if (!$detalletarea) {
            throw $this->createNotFoundException('No encuentro la tarea.');
        }

        return $this->render('IncidenciaBundle:Default:detalletarea.html.twig', array(
                'detalletarea' => $detalletarea
        ));
    }

    /**
     * Crea una entrada del blog
     */
    public function altatareaAction(Request $request)
    {

        $tareas = new Tareas();
        $form = $this->createForm(new TareasType(), $tareas, array(
                'action' => $this->generateUrl('altatarea'),
                'method' => 'POST',
        ));

        //-- En caso de que el request haya sido invocado por POST
        //   procesaremos el formulario
        if($request->getMethod() == 'POST')
        {

            $form->handleRequest($request);

            if($form->isValid())
            {
                //Actualizamos los datos
                $em = $this->getDoctrine()->getManager();
                $em->persist($tareas);
                $em->flush();

                return $this->redirect($this->generateUrl('listartareas'));
            }
        }

        return $this->render('IncidenciaBundle:Default:editartarea.html.twig', array(
                'tarea' => $tareas,
                'form'    => $form->createView(),
                'titulo'  => 'Alta de Tareas',
        ));
    }

     public function modificartareaAction(Request $request,$id)
     {

            $em = $this->getDoctrine()->getManager();
            $tarea = $em->getRepository('IncidenciaBundle:Tareas')->find($id);
            $form = $this->createForm(new TareasType(),
                                      $tarea,
                                      array('action' => $this->generateUrl('modificartarea',
                                                                            array('id' => $tarea->getId_tarea()))));

            if($request->getMethod() == 'POST')
            {

                 $form->handleRequest($request);
                //-- Con esto nuestro formulario ya es capaz de decirnos si
                //   los dato son válidos o no y en caso de ser así
                if($form->isValid())
                {

                    $em = $this->getDoctrine()->getManager();

                    $em->persist($tarea);
                    $em->flush();

                    return $this->redirect($this->generateURL('listartareas'));
                }

            }

             return $this->render('IncidenciaBundle:Default:editartarea.html.twig', array(
                'form' => $form->createView(),
                'titulo'  => 'Modificar Tarea',
            ));

    }


    public function borrartareaAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $tarea = $em->getRepository('IncidenciaBundle:Tareas')->find($id);

        if (!$tarea) {
            throw $this->createNotFoundException('no encontrada la tarea para esa id '.$id);
        }

        $em->remove($tarea);
        $em->flush();

        return $this->redirect($this->generateURL('listartareas'));
    }

    public function listarsubtareasAction()
    {

        $em = $this->getDoctrine()->getManager();
        $tarea = $em->getRepository('IncidenciaBundle:Subtareas')->getSubtareas();

        return $this->render('IncidenciaBundle:Default:listadosubtareas.html.twig', array('subtareas' => $tarea));
    }

    /**
     * Muestra una subtarea
     */
    public function detallesubtareaAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $detallesubtarea = $em->getRepository('IncidenciaBundle:Subtareas')->find($id);

        if (!$detallesubtarea) {
            throw $this->createNotFoundException('No encuentro la subtarea.');
        }

        return $this->render('IncidenciaBundle:Default:detallesubtarea.html.twig', array(
                'detallesubtarea' => $detallesubtarea
        ));
    }

    /**
     * Crea una entrada del blog
     */
    public function altasubtareaAction(Request $request)
    {

        $subtareas = new Subtareas();
        $form = $this->createForm(new SubtareasType(), $subtareas, array(
                'action' => $this->generateUrl('altasubtarea'),
                'method' => 'POST',
        ));

        //-- En caso de que el request haya sido invocado por POST
        //   procesaremos el formulario
        if($request->getMethod() == 'POST')
        {

            $form->handleRequest($request);

            if($form->isValid())
            {
                //Actualizamos los datos
                $em = $this->getDoctrine()->getManager();
                $em->persist($subtareas);
                $em->flush();

                return $this->redirect($this->generateUrl('listarsubtareas'));
            }
        }

        return $this->render('IncidenciaBundle:Default:editarsubtarea.html.twig', array(
                'subtarea' => $subtareas,
                'form'    => $form->createView(),
                'titulo'  => 'Alta de Subtareas',
        ));

    }

     public function modificarsubtareaAction(Request $request,$id)
     {

            $em = $this->getDoctrine()->getManager();
            $subtarea = $em->getRepository('IncidenciaBundle:Subtareas')->find($id);
            $form = $this->createForm(new SubtareasType(),
                                      $subtarea,
                                      array('action' => $this->generateUrl('modificarsubtarea',
                                                                            array('id' => $subtarea->getIdsubtarea()))));

            if($request->getMethod() == 'POST')
            {

                 $form->handleRequest($request);
                //-- Con esto nuestro formulario ya es capaz de decirnos si
                //   los dato son válidos o no y en caso de ser así
                if($form->isValid())
                {

                    $em = $this->getDoctrine()->getManager();

                    $em->persist($subtarea);
                    $em->flush();

                    return $this->redirect($this->generateURL('listarsubtareas'));
                }

            }

             return $this->render('IncidenciaBundle:Default:editarsubtarea.html.twig', array(
                'form' => $form->createView(),
                'titulo'  => 'Modificar Subtarea',
            ));

    }

    public function borrarsubtareaAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $subtarea = $em->getRepository('IncidenciaBundle:Subtareas')->find($id);

        if (!$subtarea) {
            throw $this->createNotFoundException('no encontrada la subtarea para esa id '.$id);
        }

        $em->remove($subtarea);
        $em->flush();

        return $this->redirect($this->generateURL('listarsubtareas'));
    }

    /**
     * @Route("/admin")
     */
    public function adminAction()
    {
        //return new Response('<html><body>Admin page!</body></html>');
        $name='acceso como administrador';

        return $this->render('IncidenciaBundle:Default:index.html.twig', array('name' => $name));

    }

    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
                    );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
                'IncidenciaBundle:Default:login.html.twig',
                array(
                        // last username entered by the user
                        'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                        'error'         => $error,
                )
                );
    }

}
