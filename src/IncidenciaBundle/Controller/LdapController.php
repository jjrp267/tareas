<?php

// src/UserBundle/Controller/LoginController.php

namespace IncidenciaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Routing\RouterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LdapController extends Controller
{
    public function indexAction(Request $request)
    {
        $arrViewData = array('USER_EMAIL' => NULL, 'PASSWORD' => NULL, 'ERROR' => NULL);

        // Checks if the login form has been submitted
        if($request->getMethod() == 'POST')
        {
            // load Ldap service
            $objLdapServ = $this->get('ldap');

            // check Ldap login
            $arrLoginResult = $objLdapServ->checkLdapLogin();

            // Ldap login result
            $arrViewData = json_decode($arrLoginResult, TRUE);

            // check Ldap login result
            if($arrViewData['LOGIN'] == "OK")
            {
                return $this->redirect($this->generateUrl('listar'));
            }
        }

        return $this->render('IncidenciaBundle:Default:ldap.html.twig', $arrViewData);
    }
}
