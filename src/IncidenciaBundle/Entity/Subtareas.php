<?php

namespace IncidenciaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Meta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IncidenciaBundle\Entity\SubtareasRepository")
 */
class Subtareas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idsubtarea", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idsubtarea;

    /**
     * @var string
     *
     * @ORM\Column(name="descsubtarea", type="string", length=20)
     */
    private $descsubtarea;

    /**
     * @ORM\Column(name="fc_modif", type="datetime")
     */
    private $fc_modif;

    /**
    * @ORM\ManyToOne(targetEntity="IncidenciaBundle\Entity\Tareas", inversedBy="members")
    * @ORM\JoinColumn(name="fk_tarea_id", referencedColumnName="id_tarea")
    */
    protected $group;

    public function __construct()
    {

        $this->setFc_modif(new \DateTime());
    }

    /**
     * Get id_tarea
     *
     * @return integer
     */
    public function getidsubtarea()
    {
        return $this->idsubtarea;
    }

    /**
     * Set ds_tarea
     *
     * @param string $descsubtarea
     * @return tarea
     */
    public function setDescsubtarea($descsubtarea)
    {
        $this->descsubtarea = $descsubtarea;
        //$this->setSlug($this->ds_tarea);
        return $this;
    }

    /**
     * Get descsubtarea
     *
     * @return string
     */
    public function getDescsubtarea()
    {
        return $this->descsubtarea;
    }

    /**
     * Set Fecha modificacion
     *
     * @param \DateTime $fc_modif
     * @return
     */
    public function setFc_modif($fc_modif)
    {
        $this->fc_modif = $fc_modif;

        return $this;
    }

    /**
     * Set Fecha modificacion
     *
     * @param \DateTime $fc_modif
     * @return
     */
    public function setFcmodif($fc_modif)
    {
        $this->fc_modif = $fc_modif;

        return $this;
    }

    /**
     * Get Fecha modificacion
     *
     * @return \DateTime
     */
    public function getFcModif()
    {
        return $this->fc_modif;
    }

    public function setGroup($group)
    {
        $this->group = $group;
    }

    public function getGroup()
    {
        return $this->group;
    }

}
