<?php

namespace IncidenciaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Meta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IncidenciaBundle\Entity\TareasRepository")
 */
class Tareas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tarea", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_tarea;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_tarea", type="string", length=20)
     */
    private $ds_tarea;

    /**
     * @ORM\OneToMany(targetEntity="IncidenciaBundle\Entity\Subtareas", mappedBy="group")
     */
    protected $members;

    /**
     * @ORM\Column(name="fc_modif", type="datetime")
     */
    private $fc_modif;

    public function __construct()
    {

        $this->setFc_modif(new \DateTime());
        $this->members = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id_tarea
     *
     * @return integer
     */
    public function getId_tarea()
    {
        return $this->id_tarea;
    }

    /**
     * Set ds_tarea
     *
     * @param string $ds_tarea
     * @return tarea
     */
    public function setDs_tarea($ds_tarea)
    {
        $this->ds_tarea = $ds_tarea;
        //$this->setSlug($this->ds_tarea);
        return $this;
    }

    /**
     * Set ds_tarea
     *
     * @param string $ds_tarea
     * @return tarea
     */
    public function setDstarea($ds_tarea)
    {
        $this->ds_tarea = $ds_tarea;
        //$this->setSlug($this->ds_tarea);
        return $this;
    }

    /**
     * Get ds_tarea
     *
     * @return string
     */
    public function getDsTarea()
    {
        return $this->ds_tarea;
    }

    /**
     * Get ds_tarea
     *
     * @return string
     */
    public function getDs_Tarea()
    {
        return $this->ds_tarea;
    }

    /**
     * Set Fecha modificacion
     *
     * @param \DateTime $fc_modif
     * @return
     */
    public function setFc_modif($fc_modif)
    {
        $this->fc_modif = $fc_modif;

        return $this;
    }

    /**
     * Set Fecha modificacion
     *
     * @param \DateTime $fc_modif
     * @return
     */
    public function setFcmodif($fc_modif)
    {
        $this->fc_modif = $fc_modif;

        return $this;
    }

    /**
     * Get Fecha modificacion
     *
     * @return \DateTime
     */
    public function getFcModif()
    {
        return $this->fc_modif;
    }

    /**
     * Get Fecha modificacion
     *
     * @return \DateTime
     */
    public function getFc_Modif()
    {
        return $this->fc_modif;
    }

    public function setMembers($members)
    {
        $this->members = $members;
    }

    public function getMembers()
    {
        return $this->members;
    }

}
