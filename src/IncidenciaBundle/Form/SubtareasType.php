<?php

namespace IncidenciaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SubtareasType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descsubtarea','Symfony\Component\Form\Extension\Core\Type\TextType',array("required"=>true,
                                                                                             'attr' => array('class'=>'form-control')
                                                                                             ))
            ->add('fc_modif','Symfony\Component\Form\Extension\Core\Type\DateType', array('widget' => 'single_text'))
            ->add('group', 'entity', array(
                'class' => 'IncidenciaBundle:Tareas',
                'property' => 'ds_tarea'))
            ->add('Aceptar',"submit",array('attr' => array('class'=>'btn btn-success')))
            ;

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IncidenciaBundle\Entity\Subtareas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'nombredelformulario2';
    }
}
