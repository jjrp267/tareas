<?php

namespace IncidenciaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TareasType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ds_tarea','Symfony\Component\Form\Extension\Core\Type\TextType',array("required"=>true,
                                                 'attr' => array('class'=>'form-control')
                                            ))
            ->add('fc_modif','Symfony\Component\Form\Extension\Core\Type\DateType', array('widget' => 'single_text',
                                                'attr' => array('class'=>'form-control')
                                            ))
            ->add('Aceptar',"submit",array('attr' => array('class'=>'btn btn-success')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IncidenciaBundle\Entity\Tareas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'nombredelformulario';
    }
}
