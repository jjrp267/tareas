CREATE TABLE `tareas` (
  `id_tarea` int(11) NOT NULL AUTO_INCREMENT,
  `ds_tarea` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fc_modif` datetime NOT NULL,
  PRIMARY KEY (`id_tarea`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

INSERT INTO `tareas` (`id_tarea`, `ds_tarea`, `fc_modif`) VALUES
(1, 'tarea 1', '2017-09-29 00:00:00'),
(2, 'tarea 2', '2017-10-04 00:00:00'),
(3, 'tarea 3', '2017-10-04 00:00:00'),
(4, 'tarea 4', '2017-10-04 00:00:00'),
(5, 'tarea 5', '2017-10-04 00:00:00'),
(6, 'tarea 6', '2017-10-04 00:00:00'),
(7, 'tarea 7', '2017-10-05 00:00:00'),
(9, 'tarea 8', '2017-10-05 00:00:00'),
(10, 'tarea 9', '2017-10-05 00:00:00');

CREATE TABLE `subtareas` (
  `idsubtarea` int(11) NOT NULL AUTO_INCREMENT,
  `descsubtarea` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fc_modif` datetime NOT NULL,
  `fk_tarea_id` int(11) NOT NULL,
  PRIMARY KEY (`idsubtarea`),
  KEY `fk_tarea_id` (`fk_tarea_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

INSERT INTO `subtareas` (`idsubtarea`, `descsubtarea`, `fc_modif`, `fk_tarea_id`) VALUES
(1, 'subtarea 4', '2017-10-05 00:00:00', 2),
(3, 'subtarea 1', '2017-10-12 00:00:00', 1),
(4, 'subtarea 2', '2017-10-05 00:00:00', 2),
(5, 'subtarea 3', '2017-10-05 00:00:00', 2);

ALTER TABLE `subtareas`
  ADD CONSTRAINT `subtareas_ibfk_1` FOREIGN KEY (`fk_tarea_id`) REFERENCES `tareas` (`id_tarea`);
