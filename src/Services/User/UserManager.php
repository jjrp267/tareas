<?php

// src/Services/User/UserManager.php

namespace Services\User;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;

class UserManager
{
    private $container, $em, $request, $session, $user;

    public function __construct(EntityManager $em, ContainerInterface $container, Request $request, Session $session)
    {
        $this->em = $em;
        $this->request = $request;
        $this->container = $container;
        $this->session = $session;
    }

    // checks if user exists when login form has been submitted
    public function loginAction($strUsername)
    {
        if( ! $this->checkUserExists($strUsername) )
        {
            // create new user
            $this->createUser($strUsername);
        }

        $this->createLoginSession();
    }

    // get user data from database
    public function getUser($strUsername)
    {
        return $this->em->getRepository('UserBundle:User')->findOneBy( array('user' => $strUsername) );
    }

    // Check if a user exists on database
    public function checkUserExists($strUsername)
    {
        $this->user = $this->getUser($strUsername);
        return ( ! empty($this->user)) ? true : false;
    }

    // Create new user on database
    public function createUser($strUsername)
    {
        $boolResult = false;
        $objCurrentDatetime = new \Datetime();

        try
        {
            $objUser = new User();
            $objUser->setUser($strUsername);
            $objUser->setCreationDate($objCurrentDatetime);
            $objUser->setLastLoginDate($objCurrentDatetime);

            // save data
            $this->em->persist($objUser);
            $this->em->flush();

            // result data
            $boolResult = true;

            // user obj
            $this->user = $objUser;
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage();
        }

        return $boolResult;
    }

    // creates login session
    public function createLoginSession()
    {
        $objToken = new UsernamePasswordToken($this->user, null, 'main', $this->user->getRoles() ) ;

        // update user last login
        $this->user->setLastLoginDate( new \Datetime() );
        $this->em->persist($this->user);
        $this->em->flush();

        // save token
        $objTokenStorage = $this->container->get("security.token_storage")->setToken($objToken);
        $this->session->set('_security_main', serialize($objToken));
    }

    // logout a user
    public function logOutUser()
    {
        $this->container->get('security.context')->setToken(null);
        $this->container->get('request')->getSession()->invalidate();

        $url = $router->generate('oportunidades');
        return $this->redirect($url);
    }

}